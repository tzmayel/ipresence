
import $ from 'jquery';
import './styles/appStyles.scss';

$.fn.myPlugin = function(opts) {
  const options = {
    counterLabelObj: null,
  };
  $.extend(options, opts);

  const maxAllowed = $(this).data('maxlenght');
  const calc = () => {
    if ($(this).val().length > maxAllowed) {
      $(this).val($(this).val().slice(0, maxAllowed));
    }

    if (options.counterLabelObj) {
      options.counterLabelObj.html(
          (maxAllowed - $(this).val().length) + ' characters left');
    } else {
      console.log(`Please set counterLabel to display remaining characters!`);
    }
  };

  $(this).bind('keyup change keydown', () => {
    calc();
  });

  calc(); // initail call
};
